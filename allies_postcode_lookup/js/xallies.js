jQuery(document).ready(function ($) {

// hide data set for UK.
$('.field-type-allies-postcode-lookup select').hide();

//search the dom for all instances of allies address field & alter the id to something more friendly
//increment the value by 1

$('.field-type-allies-address-field input').each(function( i ){
    this.id = "allies_field_address_line_" + i;
});

// on every page the postcode search is called, alter the id of the postcode field to searchField
$('.field-type-allies-postcode-lookup input').attr('id', 'searchField');

	$('#searchField').worldaddresses({
					searchURL: "http://ws.worldaddresses.com",
					searchKey: "JQMLQ-PDNRY-RKQCR-S8S5A",
					functionName: "WA_M3OTSPC",
					defaultDataset: "UK",
					outputMap: {
							fields: [
							{
								id: "organisation_name", // not used in this module
								dataIndex: 0
							},
							{
								id: "allies_field_address_line_0", //address line 1
								dataIndex: 1
							},
							{
								id: "allies_field_address_line_1", //address line 2
								dataIndex: 2
							},
							{
								id: "allies_field_address_line_2", //address line 3
								dataIndex: 3
							},
							{
								id: "allies_field_address_line_3", //town
								dataIndex: 4
							},
							{
								id: "allies_field_address_line_4", //county
								dataIndex: 5
							},
							{
								id: "searchField",  //postcode
								dataIndex: 6
							},
							{
								id: "allies_field_address_line_5", //country
								dataIndex: 7
							}
						],
						appendText: "\n"
					},
					addressPickerMap: [
						1,
						2,
						3,
						4,
						5,
						6,
						7
					],
				paging: true,
				debug: true
				});
});
// validate the search key automatically in drupal config form

/*$('#edit-search-key').keyup(function() {
  var foo = $(this).val().split("-").join(""); // remove hyphens
  if (foo.length > 0) {
    foo = foo.match(new RegExp('.{1,5}', 'g')).join("-");
  }
  $(this).val(foo);*/
//});
//});


