-- SUMMARY --

The Allies postcode lookup module provides easy integration with the Allies postcode lookup service for the UK range of postcodes. 
It adds two new field types for adding the postcode lookup field, and corresponding address fields which automatically 

For a full description of the module, visit the project page
	@to do - confirm URL

To submit bug reports and feature suggestions, or to track changes:
	@to do - confirm URL

-- REQUIREMENTS --

jQuery v1.7 or later

-- INSTALLATION --

* Install as ususal, see http://drupal.org/node/70151 for further information.

* You will need to signup for a full account at Allies computing to use the project for commercial projects: http://www.alliescomputing.com/products/world-addresses-web/trial/centralstep

-- CONFIGURATION --

* Once installed, you will need to obtain you free trial key from Allies to give you ten days unlimited usage.

* Copy and paste it into the Search Key field at admin > config > allies > postcode_lookup

* On any entity; add the postcode lookup field type. Add any address fields you require in the following order. The following are available:

* Address Line 1
* Address Line 2
* Address Line 3
* Town / City
* County
* Country


-- TROUBLESHOOTING & SUPPORT --

 * Please email support@centralstep.com with any support question or post directly in the issue queue.

* Frequently asked questions will be collated and added to the Documentation and README.txt

-- MAINTAINERS --
* Matt Galley - https://drupal.org/user/834030/


-- SPONSORSHIP --
* This module was kindly sponsored by the team at Allies Computing; creators of the postcode lookup service

* http://alliescomputing.com


